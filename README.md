# voxel-touchy

voxel-touchy is a touch adaptor for [voxel.js](http://voxeljs.com).

## Usage

```js
var createGame = require('voxel-engine')
var touchy = require('voxel-touchy')

// create a game
var game = createGame()

// touch enable the game
var touches = touchy(game)

// pipe touch controls into the game controls layer
touches.pipe(game.controls)
touches.rotation.pipe(game.controls.createWriteRotationStream())
```

## Overview and Control Style

The control style of voxel-touchy is a little different to other mobile gamepad implementations.  The general concept is that there are **no fixed regions** of the screen that are reserved for directional control, but rather wherever you touch is going to become your gamepad.

The first touch on the screen becomes the movement control which is responsbile for move forward, backward, left and right. The second touch on th escreen becomes the look control which is responsible for look up, down, left and right.

This creates opportunities for some different use cases on a mobile device:

1. Holding the device in one hand and using a single finger to browse the game world, implements the movement actions.  

2. Holding the device in two hands using thumbs for interaction, the first thumb on the screen will implement movement with the second thumb touching the screen implementing look.

## TODO

- Implement jump (thinking a quick swipe up at this stage)
- Implement action (single tap)
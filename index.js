var inherits = require('inherits'),
    Stream = require('stream').Stream;

function TouchyVoxels(game, opts) {
    // get a reference to the manipulator
    var touchy = this;

    // call the inherited stream constructer
    Stream.call(this);

    // initialise opts
    opts = opts || {};

    // set the stream as readable
    this.readable = true;

    // save a reference to the game and element
    this.game = game;

    // capture the touch events for the game element
    ['start', 'move', 'end'].forEach(function(eventType) {
        var touchHandler = touchy['touch' + eventType];

        // capture touch changes
        game.element.addEventListener('touch' + eventType, function(evt) {
            var touches = [],
                changed = [],
                ii;

            for (ii = evt.changedTouches.length; ii--; ) {
                touches[ii] = evt.touches[ii];
            }

            for (ii = evt.changedTouches.length; ii--; ) {
                changed[ii] = evt.changedTouches[ii];
            }

            touchHandler.call(touchy, evt, touches, changed);
        }, false);
    });

    // initialise the active touches array
    this.activeTouches = [];
    this.mId = undefined;
    this.lId = undefined;

    // initialise the dead movement zone
    this.deadzone = typeof opts.deadzone != 'undefined' ? opts.deadzone : 15;
    this.lookDamping = opts.lookDamping || 0.2;
    this.invertY = typeof opts.invertY == 'undefined' || opts.invertY;

    // save the move stick last positions
    this.moveOrigin = undefined;
    this.lookOrigin = undefined;

    // initialise the rotation stream
    this.rotation = new Stream();
 }

/**
## touchstart

Initial touch handler - which will capture our movement (not look) controls
*/
TouchyVoxels.prototype.touchstart = function(evt, active, changed) {
    var touchy = this,
        leftMost = this.game.element.offsetWidth,
        currentTouchCount = this.activeTouches.filter(function(touch) {
            return typeof touch != 'undefined';
        }).length,
        moverTouch,
        nonMoveTouches;

    // if we have 0 current touches, then we have to assign the movement stick
    if (! this.moveOrigin) {
        // if the have only one changed touch, then it's going to be it
        if (active.length === 1) {
            moverTouch = active[0];
        }
        // otherwise, find the left-most touch
        // TODO: support left handers
        else {
            active.forEach(function(touch, index) {
                if ((! moverTouch) || (touch.pageX < moverTouch.pageX)) {
                    moverTouch = touch;
                }
            });
        }

        // use the mover touch
        this.mId = moverTouch.identifier;
        this.moveOrigin = {
            x: moverTouch.pageX,
            y: moverTouch.pageY
        };
    }

    // find the non-move touches
    nonMoveTouches = changed.filter(function(touch) {
        return touch.identifier !== touchy.mId;
    });

    // if we do not have a look origin specified, then initailize
    if (nonMoveTouches.length > 0 && (! this.lookOrigin)) {
        this.lookOrigin = {
            x: nonMoveTouches[0].pageX,
            y: nonMoveTouches[0].pageY
        };

        this.lId = nonMoveTouches[0].identifier;
    }
};

/**
## touchMove(evt, active, changed)
*/
TouchyVoxels.prototype.touchmove = function(evt, active, changed) {
    var touchy = this,
        currentMove = this.activeTouches[this.mId] || this.moveOrigin,
        currentLook = this.activeTouches[this.lId] || this.lookOrigin,
        moveDeltaX, moveDeltaY,
        lookDeltaX, lookDeltaY,
        data,
        changes = {};

    // prevent default movement
    evt.preventDefault();

    // apply movement to the touches
    active.forEach(function(touch) {
        touchy.activeTouches[touch.identifier] = {
            x: touch.pageX,
            y: touch.pageY
        };
    });

    // calculate the move delta
    if (this.moveOrigin) {
        moveDeltaX = currentMove.x - this.moveOrigin.x;
        moveDeltaY = this.moveOrigin.y - currentMove.y;

        if (Math.abs(moveDeltaX) > this.deadzone) {
            this.xCommand = moveDeltaX > 0 ? 'right' : 'left';
            changes[this.xCommand] = 1;
        }
        else if (this.xCommand) {
            changes[this.xCommand] = 0;
            this.xCommand = undefined;
        }

        if (Math.abs(moveDeltaY) > this.deadzone) {
            this.yCommand = moveDeltaY > 0 ? 'forward' : 'backward';
            changes[this.yCommand] = 1;
        }
        else if (this.yCommand) {
            changes[this.yCommand] = 0;
            this.yCommand = undefined;
        }        
    }

    // if the look origin is set then apply changes there
    if (this.lookOrigin && currentLook && this.rotation) {
        data = {
            dx: (currentLook.x - this.lookOrigin.x) * this.lookDamping,
            dy: (this.lookOrigin.y - currentLook.y) * this.lookDamping
        };

        if (this.invertY) {
            data.dy = data.dy * -1;
        }

        if (data.dx || data.dy) {
            this.rotation.emit('data', data);
        }
    }

    if (Object.keys(changes).length) {
        this.emit('data', changes);
    }
};

/**
## touchend(evt, active, changed)
*/
TouchyVoxels.prototype.touchend = function(evt, active, changed) {
    var touchy = this,
        data = {},       
        moveTouch = changed.filter(function(touch) {
            return touch.identifier === touchy.mId;
        })[0],
        lookTouch = changed.filter(function(touch) {
            return touch.identifier === touchy.lId;
        })[0];

    // remove any changed touches
    changed.forEach(function(touch) {
        touchy.activeTouches[touch.identifier] = undefined;
    });

    // reset the move touch if untouching...
    if (moveTouch) {
        // reset the forward back movement
        data.forward = 
        data.backward = 
        data.left = 
        data.right = 0;

        // reset the xcommand, ycommand, etc
        this.xCommand = 
        this.yCommand = 
        this.moveOrigin = undefined;
    }

    // if the look touch is being removed, then reset
    if (lookTouch) {
        // reset the look origin
        this.lookOrigin = undefined;
    }

    if (Object.keys(data).length) {
        this.emit('data', data);
    }    
};

inherits(TouchyVoxels, Stream);

module.exports = function(game) {
    return new TouchyVoxels(game);
};